
## Introduction

Atla-Vue is [Atila](https://pypi.org/project/atila/) extension package for
using [vue3-sfc-loader](https://github.com/FranckFreiburger/vue3-sfc-loader)
and [Bootstrap 5](https://getbootstrap.com/).

It will be useful for building simple web service at situation frontend developer
dose not exists.

Due to the [vue3-sfc-loader](https://github.com/FranckFreiburger/vue3-sfc-loader),
We can use **vue single file component** on the fly without any compiling or
building process.

Atila-Vue composes these things:

- VueJS 3
- VueRouter
- Vuex
- PWA
- Optional Bootstrap 5 for UI/UX
- Optional Firebase Authentification, Cloud Messaging

For injecting objects to Vuex, it uses [Jinja2](https://jinja.palletsprojects.com) template engine.

### Why Do I Need This?
- Single stack frontend developement, No 500M `node_modules`
- Optimized multiple small SPA/MPAs in single web service
- SSR and SEO advantage

### Full Example
See [atila-vue](https://gitlab.com/atila-ext/atila-vue) repository and [atila-vue examplet](https://gitlab.com/atila-ext/atila-vue/-/tree/master/example).




















## Configure Service Worker Caching
Place `sw-config.js` into your website root.
```js
const serviceWorkerConfig = {
    DEV_HOSTS: ["dev.myservice.com"], // localhost are default
    LOCAL_CACHE_VERSION: "<<COMMIT_SHA>>", // replace this value at every deploy
    CORS_CACHE_VERSION: "v1", // external cors cache changing is barely required
}
```
Be careful add DEV_HOSTS otherwise your site will not be reloaded because of cache.

Also place `sw.js` into your website root only if you need to overwrite default `sw.js`.
















## Launching Server
```shell
mkdir myservice
cd myservice
```

`skitaid.py`
```python
#! /usr/bin/env python3
import skitai
import atila_vue
from atila import Allied
import backend

if __name__ == '__main__':
    with skitai.preference () as pref:
      skitai.mount ('/', Allied (atila_vue, backend), pref)
    skitai.run (ip = '0.0.0.0', name = 'myservice')
```

`backend/__init__.py`
```python
import skitai

def __config__ (pref):
    pref.set_static ('/', skitai.joinpath ('backend/static'))
    pref.config.MAX_UPLOAD_SIZE = 1 * 1024 * 1024 * 1024

def __app__ ():
    import atila
    return atila.Atila (__name__)

def __mount__ (context, app):
    import atila_vue

    @app.route ("/api")
    def api (context):
        return {'version': atila_vue.__version__}

    @app.route ("/ping")
    def ping (context):
        return 'pong'
```

Now you can startup service.
```shell
./serve/py --devel
```
Then your browser address bar, enter `http://localhost:5000/ping`.







## Site Template
`backend/templates/site.j2`
```jinja
{% extends 'atila-vue/bs5.j2' %}
{% block lang %}en{% endblock %}
```






## Multi Page App

This will expose the essential contents which human and web crawler
can read directly at page source, because SEO reason or something.

Use Jinja2 template engine for essential SEO content. And use Vue
for human interactable UX.

`backend/__init__.py`
```python
def __mount__ (context, app):
  import atila_vue
  @app.route ('/')
  @app.route ('/mpa')
  def mpa (context):
      article = "..."
      return context.render (
          'mpa.j2',
          blog_title = "Title"
          blog_content = article
      )
```

`backend/templates/mpa.j2`
```jinja
{% extends 'site.j2' %}
{% block content %}
    {% include 'includes/header.j2' %}
    <div class="container">
      <h1>{{ context.blog_title }}</h1>
      <article>{{ context.blog_article }}</article>
      <div>{{ "current_time"|vue }}</div>
    </div>
{% endblock content %}

<script>
{% block vue_setup %}
  current_time = ref (new Date ())
  return { current_time }
{% endblock vue_setup %}
</script>
```

### Using SFC Components
You can still use SFC as component,

```python
def __mount__ (context, app):
  @app.route ('/mpa')
  def mpa (context):
      return context.render (
          'mpa.j2',
          vue_config = dict (use_loader = True), # add this line
          version = atila_vue.__version__
      )
```

```jinja
{% block content %}
    ...
    <sub-component></sub-component>
{% endblock content %}

{% block vue_components %}
  {{ add_component ('routes/spa/components/subComponent.vue') }}
{% endblock %}
```






## Single Page App
`backend/__init__.py`
```python
def __mount__ (context, app):
  import atila_vue
  @app.route ('/spa/<path:path>')
  def spa (context, path = None):
      return context.render (
          'spa.j2',
          vue_config = dict (
              use_router = context.baseurl (spa),
              use_loader = True
          ),
          version = atila_vue.__version__
      )
```

`backend/templates/spa.j2`
```jinja
{% extends 'site.j2' %}
{% block content %}
    {% include 'includes/header.j2' %}
    {{ super () }}
{% endblock %}
```

As creating vue files, vue-router will be automatically configured.
- `backend/static/routes/spa/index.vue`: /spa
- `backend/static/routes/spa/sub.vue`: /spa/sub
- `backend/static/routes/spa/items/index.vue`: /spa/items
- `backend/static/routes/spa/items/_id.vue`: /spa/items/:id


### App Layout
`backend/static/routes/spa/layout.vue`
```html
<template>
  <div class="container">
    <transition name="slide" mode="out-in" appear>
      <AppBar v-show="route.path.indexOf ('-fsv') == -1" class="no-print"></AppBar>
    </transition>

    <div class="py-2" id="main-content">
      <router-view v-slot="{ Component }">
          <error-boundary>
            <template v-slot:error>
              <error-template />
            </template>
            <transition name="fade" mode="out-in" appear>
              <keep-alive>
                <component :is="Component" />
              </keep-alive>
            </transition>
          </error-boundary>
      </router-view>
    </div>
  </div>
</template>

<script setup>
  import ErrorBoundary from "/atila-vue/components/error-boundary.vue"
  import ErrorTemplate from "./components/error-template.vue"

  const route = useRoute ()
</script>
```


### Optional Component To Use In Pages
`backend/static/routes/spa/components/myComponent.vue`
```html
<template>
  My Component
</template>

<script setup>
</script>
```


### Route Pages
`backend/static/routes/spa/index.vue`
```html
<template>
  <div class="container">
    <h1>Main Page</h1>
    <span class="example">
      <i class="bi-alarm"></i>{{ msg }}</span>
    <div><router-link :to="{ name: 'sub'}">Sub Page</router-link></div>
    <div><router-link :to="{ name: 'items'}">Items</router-link></div>
    <div><my-component></my-component></div>
  </div>
</template>

<script setup>
  import myComponent from '/routes/spa/components/myComponent.vue'
  const msg = ref ('hello world!')
</script>
```


`backend/static/routes/spa/sub.vue`
```html
<template>
  <div class="container">
    <h1>Sub Page</h1>
    <div><router-link :to="{ name: 'index'}">Main Page</router-link></div>
  </div>
</template>
```


`backend/static/routes/spa/items/index.vue`
```html
<template>
  <div class="container">
    <h1>Items</h1>
    <ul>
      <li v-for="index in 100" :key="index">
        <router-link :to="{ name: 'items/:id', params: {id: index}}">Item {{ index }}</router-link>
      </li>
    </ul>
  </div>
</template>
```


`backend/static/routes/spa/items/_id.vue`
```html
<template>
  <div class="container">
    <h1 class='ko-b'>Item {{ item_id }}</h1>
  </div>
</template>

<style scoped>
  .example {
    color: v-bind('color');
  }
</style>

<script setup>
  const route = useRoute ()
  const item_id = ref (route.params.id)
  onActivated (() => {
    item_id.value = route.params.id
  })
  watch (() => item_id.value,
    (to, from) => {
      log (`item changed: ${from} => ${to}`)
    }
  )
</script>

<script>
  export default {
   beforeRouteEnter (to, from, next) {
      next ()
    }
  }
</script>
```










## Using Vuex: Injection

### Adding States
`backend/templates/mpa.j2`.
```jinja
{% extends '__framework/bs5.j2' %}

{% block state_map %}
  {{ map_state ('page_id', 0) }}
  {{ map_state ('types', ["todo", "canceled", "done"]) }}
{% endblock %}
```

These will be injected to `Vuex` through JSON.

Now tou can use these state on your vue file with `useStore`.
```html
<template>
  <div>{{ state.page_id }}</div>
</template>

<script setup>
  const { state } = useStore ()
  const initial_types = ref ([...state.types])
</script>
```

#### From Keyword Arguments
```jinja
{% state_map %}
  {{ map_kwargs ('myvar', a = 2, b = 3) }}
{% endblock %}
```
As a result,
```json
state.myvar = {a: 2, b: 3}
```


Below macros are for saving source page bytes and keeping data
consistent structure. These will extract data from visible HTML tags.

#### From Tag Data
```jinja
{% block content %}
  <div id="title">Title</div>
{% endblock %}

{% state_map %}
  {{ map_text ('myvar', '#title') }}
{% endblock %}
```

As a result,
```json
state.myvar = "Title"
```

`map_html ()` is very same except data is 'innerHTML'.


#### From `data-` Attributes
```jinja
{% block content %}
<ul>
{% for row in articles %}
  <li class="article" data-id="{{row.id}}">{{ row.title }}</li>
{% endfor %}
</ul>
{% endblock %}

{% state_map %}
  {{ map_data_attrs ('articles', "ul .article", 2) }}
{% endblock %}
```

As a result, Vuex store has `articles` state and we can get `id` of each list items.
```json
state.articles = [{id: 100}, {id: 101}]
```



### Cloaking Control

Hide contents until page is ready.

`backend/templates/mpa.j2`.
```jinja
{% extends '__framework/bs5.j2' %}

{% block state_map %}
    {{ set_cloak (True) }}
{% endblock %}
```

`index.vue` or nay vue
```html
<script setup>
onMounted (async () => {
  await sleep (10000) // 10 sec
  set_cloak (false)
})
</script>
```
















## Authorization And Access Control

### Basic About API

#### Adding API
`backend/services/apis.py`
```python
def __mount__ (app. mntopt):
  @app.route ("")
  def index (context):
    return "API Index"

  @app.route ("/now")
  def now (context):
    return context.API (result = time.time ())
```

Create `backend/services/__init__.py`
```python
def __setup__ (app. mntopt):
  from . import apis
  app.mount ('/apis', apis)
```

Then update `backend/__init__.py` for mount `services`.
```python
def __app__ ():
    return atila.Atila (__name__)

def __setup__ (context, app):
    from . import services
    app.mount ('/', services)

def __mount__ (context, app):
    @app.route ('/')
    def index (context):
        return context.render ('main.j2')
```

Now you can use API: http://localhost:5000/apis/now.


#### Accessing API
```html
<script setup>
  const msg = ref ('Hello World')
  const server_time = ref (null)
  onBeforeMount ( () => {
    const r = await axios.get ('/apis/now')
    server_time.value = r.data.result
  })
</script>
```


Vuex.state has `$apispecs` state and it contains all API specification of server side. We made only 1 APIs for now.

**Note** that your exposed APIs endpoint should be `/api`.
```js
{
  APIS_NOW: { "methods": [ "POST", "GET" ], "path": "/apis/now", "params": [], "query": [] }
}
```
You can make API url by `backend.endpoint` helpers by `API ID`.
```js
const endpoint = backend.endpoint ('APIS_NOW')
// endpoint is resolved into '/apis/now'
```


### Access Control

#### Creating Server Side Token Providing API

Update `backend/__init__.py`.
```python
def __config__ (pref):
  app.secret_key = '<MY_SECRET_KEY>'

def __setup__ (context, app):
  from atila_vue import auth
  from atila_vue.auth import handler

  class MyAuthHandler (handler.ExampleAuthHandler):
      pass

  app.config.AUTH_HANDLER = MyAuthHandler (3600 * 6, 3600 * 24 * 21)
  app.mount ("/api-auth", auth)
```

Now your `state.$apispecs` is like this:
```js
{
  APIS_NOW: { "methods": [ "POST", "GET" ], "path": "/apis/now", "params": [], "query": [] },

  ATILA_VUE_AUTH_ACCESS_TOKEN: { "methods": [ "POST", "OPTIONS" ], "path": "/apis/access_token", "params": [], "query": [ "refresh_token" ] },

  ATILA_VUE_AUTH_SIGNIN_WITH_EMAIL_AND_PASSWORD: { "methods": [ "POST", "OPTIONS" ], "path": "/api-auth/signin_with_email_and_password", "params": [], "query": [ "uid", "password" ] },

  ATILA_VUE_AUTH_CREATE_USER_WITH_EMAIL_AND_PASSWORD: { "methods": [ "POST", "OPTIONS" ], "path": "/api-auth/create_user_with_email_and_password", "params": [], "query": [ "uid", "password" ] }
}
```




#### Adding Firebase Backend Service
```shell
pip3 install firebase_admin
```

`backend/__init__.py`

```python
def __config__ (context, app):
  pref.config.GOOGLE_APPLICATION_CREDENTIALS = skitai.joinpath (".google-credential.json")
```


Now your `state.$apispecs` has addtional endpoints:
```js
{
  ATILA_VUE_AUTH_FIREBASE_CUSTOM_TOKEN: { "methods": [ "OPTIONS", "POST" ], "path": "/api-auth/firebase/custom_token", "params": [], "query": [ "provider", "access_token", "payload" ] },

  ATILA_VUE_AUTH_SIGNIN_WITH_FIREBASE_ID_TOKEN: { "methods": [ "OPTIONS", "POST" ], "path": "/api-auth/firebase/signin_with_id_token", "params": [], "query": [ "id_token", "decoded_token" ] }
}
```





#### Adding Firebase Frontend Services

`backend/static/firebase-config.js`
```js
const firebaseConfig = {
    apiKey: "AIzaSyBMX8wyopi8PRx-0GecxqSyHLo8oQDFnjU",
    authDomain: "atila-vue.firebaseapp.com",
    projectId: "atila-vue",
    storageBucket: "atila-vue.appspot.com",
    messagingSenderId: "289791278372",
    appId: "1:289791278372:web:3c56d95d8e673421ad87b1",
    measurementId: "G-8VZN8WX0GF"
}
```
`firebase-config.js` should be static root which is defined `pref.set_static ('/', skitai.joinpath ('backend/static'))`.

http://localhostt:5000/firebase-config.js must respond 200 OK.

As a result, it will create javascript namespace `firebase`.

`firebase.auth`
- createUserWithEmailAndPassword
- signInWithEmailAndPassword, ...

##### Adding Analytics Service
Add `measurementId` to `backend/static/firebase-config.js`
```js
const firebaseConfig = {
    ...,
    measurementId: "G-8VZN8WX0GF"
}
```
`firebase.analytics`
- logEvent, ...


##### Adding Firebase Cloud Messaging Service
Add `vapidKey` to `backend/static/firebase-config.js`
```js
const firebaseConfig = {
    ...,
    vapidKey: "BAxq5Pb4xH5O8YgGDan2WRP4rC8Ov4W-jNvNDKNiHo5MzI2g1eopeLvy0PyS7t9-T9nLbaxH2CRpzbFQwcURYic"
}
```

`firebase.messaging`
- getToken
- onMessage,...


For your convinience, firebase has these async functions:
- `async firebase.getAuth ()`
- `async firebase.getMessaging ()`
- `async firebase.getAnalytics ()`



Vue example:
```html
<script setup>
  let messaging = null
  let auth = null

  onMounted (() => {
    messaging = await firebase.getMessaging ()
    firebase.messaging.onMessage (messaging, (msg) => {
      alert (`${msg.data.title}\n\n${msg.data.body}`)
    })

    auth = await firebase.getAuth ()
    firebase.auth.onAuthStateChanged (auth, (user) => {
      if (user) {
        msg.value = `Welcome ${user.email}`
        console.log (user)
      } else {
        msg.value = `Good bye`
      }
    })
  })

  async function signUp () {
    let userCredential = null
    try {
      userCredential = await firebase.auth.createUserWithEmailAndPassword (auth, 'hansroh@example.com', 'password')
    } catch (error) {
      traceback (error)
    }
    console.log (userCredential)
  }

  async function signOut () {
    try {
      await firebase.auth.signOut (auth)
    } catch (error) {
      traceback (error)
    }
    console.log ('sign Out Success')
  }

  async function signIn () {
    let userCredential = null
    try {
      userCredential = await firebase.auth.signInWithEmailAndPassword (auth, 'hansroh@example.com', 'password')
    } catch (error) {
      traceback (error)
    }
    console.log (userCredential)
    let response = null
    response = await backend.post (
      'ATILA_VUE_AUTH_SIGNIN_WITH_FIREBASE_ID_TOKEN',
      {id_token: userCredential.user.accessToken}
    )
    console.log (response)
  }


  async function getToken () {
    let currentToken = state.$ls.get ('FCM_TOKEN')
    if (!currentToken) {
      log ('request new FCM token')
      currentToken = await firebase.messaging.getToken (messaging)
      state.$ls.set ("FCM_TOKEN", currentToken)
    }
    log (`token: ${currentToken}`)
    console.log (currentToken)
    backend.post ("ATILA_VUE_AUTH_SEND_FCM_MESSAGE", {
      token: currentToken,
      title: "FCM Title", body: "This is FCM Message", link: "/spa"
    })
  }

</script>
```






#### Client Side Page Access Control

We provide user and grp base page access control.
```html
<script>
  export default {
    beforeRouteEnter (to, from, next) {
      permission_required (['staff'], {name: 'signin'}, {to, from, next})
    }
  }
</script>
```
`admin` and `staff` are pre-defined reserved grp name.

Vuex.state contains `$uid` and `$grp` state. So `permission_required` check with
this state and decide to allow access.

And you should build sign in component `signin.vue`.

Create `backend/static/routes/main/signin.vue`.
```js
<template>
  <div>
      <h1>Sign In</h1>
      <input type="text" v-model='uid'>
      <input type="password" v-model='password'>
      <button @click='signin ()'>Sign In</button>
  </div>
</template>

<script setup>
  const { state } = useStore ()
  if (!!state.$uid) {
    alert ('Permission Denied')
    history.back ()
    return
  }
  const uid = ref ('')
  const password = ref ('')
  async function signin () {
    const msg = await backend.get_access_and_refresh_tokens (
        'APIS_AUTH_SIGNIN_WITH_ID_AND_PASSWORD',
        {uid: uid.value, password: password.value}
    )
    if (!!msg) {
        return alert (`Sign in failed because ${ msg }`)
    }
    alert ('Sign in success!')
    permission_granted () // go to origin route
  }
</script>
```



And one more, update `/backend/static/routes/main/layout.vue`
```js
<script setup>
  backend.refresh_access_token ('APIS_ACCESS_TOKEN')

  onMounted (async () => {
    fill_screen ("#main-content", 130, 0)
  })

  watch (() => route.fullPath, () => {
    fill_screen ("#main-content", 130, 0)
  })
</script>
```
This will check saved tokens at app initializing and do these things:
- update `Vuex.state.$uid` and `Vuex.state.$grp` if access token is valid
- if access token is expired, try refresh using refresh token and save credential
- if refresh token close to expiration, refresh 'refresh token' itself
- if refresh token is expired, clear all credential

From this moment, `axios` monitor `access token` whenever you call APIs and automatically managing tokens.

Then we must create 2 APIs - API ID `APIS_SIGNIN_WITH_ID_AND_PASSWORD` and
`APIS_AUTH_ACCESS_TOKEN`.






#### Server Side Access Control

Pre-defined group names are `admin`, `staff`, and `owner`.

`admin` has root privilege and always get permission for any required permission.

```python
def __mount__ (context, app):
  @app.route ('/profiles/<uid>')
  @app.permission_required (['user'])
  def get_profile (context):
    icanaccess = context.request.user.uid
    return context.API (profile = data)
```

If request user is one of `user`, `staff` and `admin` grp, access will be granted.

And all claims of access token can be access via `context.request.user` dictionary.

`@app.permission_required` can `groups` and `owner` based control.

Also `@app.login_required` which is shortcut for `@app.permission_required ([])` - any groups will be granted.

`@app.identification_required` is just create `context.request.user` object using access token only if token is valid.

For more detail access control. see [Atila](https://pypi.org/project/atila/).


























## Appendix

### Jinja Template Helpers

#### Globals
- `raise`
- `http_error (status, *args)`: raise context.HttpError

#### Filters
- `vue (val)`
- `summarize (val, chars = 60)`
- `attr (val)`
- `upselect (val, *names, **upserts)`
- `tojson_with_datetime (data)`

#### Macros
- `component (path, alias = none, _async = True)`
- `global_component (path, alias = none, _async = True)`


#### State Injection Macros
- `map_state (name, value)`
- `map_kwargs (name, **kargs)`
- `map_data_attrs (name, css, list_size)`
- `map_text (name, css)`
- `map_html (name, css)`
- `set_cloak (flag = True)`
- `map_route (**kargs)`




### Javascript Helpers

#### Storage
- `state.$idb`: Indexed DB
- `state.$ls`: Local Storage
- `state.$ss`: Session Storage


#### Prototype Methods
- `Number.prototype.format`
- `String.prototype.format`
- `String.prototype.titleCase`
- `Date.prototype.unixepoch`
- `Date.prototype.format`
- `String.prototype.repeat`
- `String.prototype.zfill`
- `Number.prototype.zfill`

#### Service Worker Sync
- `async swsync.add_tag (tag, min_interval_sec = 0)`
- `async swsync.unregister_tag (tag)`: periodic only
- `async swsync.periodic_sync_enabled ()`
- `async swsync.is_tag_registered (tag)`

#### Backend URL Building and Authorization
- `backend.get (url, params)`
- `backend.download (url, params)`
- `backend.delete (url, params)`
- `backend.post (url, data)`
- `backend.patch (url, data)`
- `backend.put (url, data)`
- `backend.static (relurl)`
- `backend.media (relurl)`
- `async backend.get_access_and_refresh_tokens (endpoint, payload)`
- `async backend.refresh_access_token (endpoint)`: In `onBeforeMount` at `layout.vue`
- `backend.create_websocket (API_ID, read_handler = (evt) => log (evt.data))`
  - `push (msg)`


#### Logging
- `frontend.log (msg, type = 'info')`
- `frontend.traceback (e)`

#### Utilities
- `frontend.permission.required (permission, redirect, next)`: In `beforeRouteEnter`
- `frontend.permission.granted ()`: go to original requested route after signing in
- `frontend.build_url (baseurl, params = {})`
- `frontend.push_alarm (title, message, icon, timeout = 5000)`
- `frontend.load_resources (namespace, urls, callback = () => {})`: load CDN js and css
- `frontend.set_cloak (flag)`
- `async frontend.wait_for_define (obj, interval = 100, timeout = 1000)`: obj is string
- `async frontend.wait_for_true (obj, interval = 100, timeout = 1000)`: obj is string
- `async frontend.sleep (ms)`
- `async frontend.refit_height (css, bottom_margin = 0, initial_delay = 0)`
- `async frontend.refit_width (css, right_margin = 0, initial_delay = 0)`
- `async frontend.fill_screen (css, bottom_margin = 0, initial_delay = 0)`

#### Device Detecting
- `frontend.device.android`
- `frontend.device.ios`
- `frontend.device.mobile`
- `frontend.device.touchable`
- `frontend.device.rotatable`
- `frontend.device.width`
- `frontend.device.height`




#### Bootstrap UI Helpers
##### `bootstrap.popover (activator_css, content_css, option)`
```js
bootstrap.popover ('#codelist', '#codelist-content', {
  class_: "codelist-popover"
})
```

option:
- title
- placement: auto | top | bottom | left | right
- class_


##### `bootstrap.modal (option)`
```js
function gotoSignIn () {
  bootstrap.modal ()
  router.push (`/members/signin?email=${payload.value.email}`)
}

bootstrap.modal ({
  title: "Unable To Sign Up",
  text: errorMessage,
  icon: "error",
  buttons: [{text: "Sign In", class_: "btn-primary", click: gotoSignIn }],
  closeButtonText: 'Retry'
})
```
option:
- title
- text
- icon: `ok | error | warning | question | info`
- closeButtonText
- buttons: Array of Object
  - text
  - class_
  - run: function
  - to: router url


##### `bootstrap.wait (option)`
```js
function gotoSignIn () {
  bootstrap.wait ({title: 'Wait Please'})
  do_something ()
  bootstrap.wait ()
}

bootstrap.wait ({
  title: "Unable To Sign Up",
  text: errorMessage
})
```
option:
- title
- text


##### `bootstrap.toast (option)`: Bottom right toast with optional buttons
Very same as `bootstrap.modal`.

```js
bootstrap.toast ({
    title: "Success", text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    buttons: [
      {text: "Possible", class_: 'btn-primary', to: '/members/signin'},
      {text: "Sign In", class_: 'btn-info', run: () => {myfunction ()}}
    ]
})
```

option:
- title
- subTitle
- text
- icon: `ok | error | warning | question | info`
- closeButtonText
- textConfirm: text to type by user
- autoHide: false | int ms
- buttons: Array of Object
  - text
  - class_
  - run: function
  - to: url
  - needTextConfirm (if only 1 button, if can be optional)


##### `bootstrap.flash (option)`: Bootstrap alert, `<div class="alert-container">` is required

```js
bootstrap.flash ({
  text: 'Lorem ipsum dolor sit amet',
  class_: 'alert-danger',
  autoHide: 3000
})

bootstrap.flash ({
  text: 'Lorem ipsum dolor',
  class_: 'alert-primary', icon: 'ok',
  buttons: [
    {text: "Possible", class_: 'btn-primary', to: '/members/signin'},
    {text: "Sign In", class_: 'btn-primary', run: () => {a ()}}
  ]
})
```

option:
- text
- icon: `ok | error | warning | question | info`
- class_: like `alert-primary`
- buttons: Array of Object
  - text
  - class_
  - run: function
  - to: url
- autoHide: false | int ms
