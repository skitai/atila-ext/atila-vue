__version__ = "0.4.0"

import os
import json
import atila
import skitai
from skitai.wastuff.api import ISODateTimeWithOffsetEncoder
import re
import sys
from atila import Atila, Context
from . import email
try:
    # connection unexpectedly / already closed
    from django.db.utils import InterfaceError, OperationalError
    CHECK_DB_ERROR = True
except:
    CHECK_DB_ERROR = False

RX_SPACE = re.compile (r'\s+')
ROUTES_CACHE = {}
VIEWS_DIR = "/routes"
FIREBASE_CONFIG_JS = "firebase-config.js"

class User:
    def __init__ (self, claims):
        self.claims = claims
        self.uid = claims ['uid']
        self.grp = claims ['grp']
        self.tuid = None

    def __getattr__ (self, attr):
        try:
            return self.claims [attr]
        except KeyError:
            raise AttributeError

    def __str__ (self):
        return str (self.uid)


def __config__ (pref):
    pref.config.MINIFY_HTML = 'strip'
    if os.getenv ('SMTP'):
        pref.config.SMTP = email.set_default_smtp (os.getenv ('SMTP'))


def __error__ (context, app, expt):
    if not CHECK_DB_ERROR:
        return

    error_class, error_msg = expt [0], str (expt [1])
    if error_class is InterfaceError or (error_class is OperationalError and 'connection unexpectedly' in error_msg):
        context.log ('db connection failed, restarting worker...', 'error')
        context.restart ()


def __setup__ (context: Context, app: Atila):
    from skitai.wastuff import sendmail

    if "STATIC_ROOT" in app.config:
        fc = os.path.join (app.config.STATIC_ROOT, FIREBASE_CONFIG_JS)
        if os.path.isfile (fc):
            app.config.FIREBASE_CONFIG = FIREBASE_CONFIG_JS

    if "GOOGLE_APPLICATION_CREDENTIALS" in app.config:
        from firebase_admin import credentials
        import firebase_admin

        if "FIREBASE_INITIALIZED" not in app.config:
            cred = credentials.Certificate (app.config.GOOGLE_APPLICATION_CREDENTIALS)
            firebase_admin.initialize_app (cred)
            app.config.FIREBASE_INITIALIZED = True


    @app.permission_handler
    def permission_handler (context, perms):
        if not context.request.get_header ('authorization'):
            raise context.HttpError ("401 Unauthorized")

        context.request.user = None
        claims = context.decode_jwt ()
        if "err" in claims:
            raise context.HttpError ("401 Unauthorized", claims ["err"])
        context.request.user = User (claims)
        if isinstance (context.request.user.grp, str):
            context.request.user.grp = [context.request.user.grp]

        if 'uid' in context.request.args:
            tuid = context.request.args ['uid']
            context.request.user.tuid = (tuid == 'me' and context.request.user.uid or (tuid != 'notme' and tuid or None))

        if not perms:
            return
        if "admin" in context.request.user.grp:
            return # always vaild
        if "owner" in perms and context.request.user.tuid == context.request.user.uid:
            return
        if "staff" in perms and 'staff' in context.request.user.grp:
            return
        for perm in perms:
            if perm in context.request.user.grp:
                return
        raise context.HttpError ("403 Permission Denied")

    @app.route ('/status')
    @app.permission_required (skitai.isdevel () and ['user'] or ['staff'])
    def status (context, f = ''):
        return context.status (f)

    # template globals ------------------------------------
    @app.template_global ('raise')
    def raise_helper (context, msg):
        raise Exception (msg)

    @app.template_global ('http_error')
    def http_error (context, status, *args):
        raise context.HttpError (status, *args)

    @app.template_global ('get_apispecs')
    def get_apispecs (context):
        return { k.replace ('.', '_').replace (':', '_').upper (): v for k, v in sorted (app.get_urlspecs ().items (), key = lambda x: x [0]) if v ['path'].startswith ('/api') }

    @app.template_global ('build_routes')
    def build_routes (context, base, prefix):
        if prefix == '/':
            prefix = ''

        def collect (start, base_len):
            pathes = []
            for fn in os.listdir (start):
                path = os.path.join (start, fn)
                if fn in ('layout.vue',):
                    continue
                if fn.startswith ('__'):
                    continue

                if os.path.isdir (path):
                    pathes.extend (collect (path, base_len))
                elif fn.endswith ('.vue'):
                    pathes.append ((
                        path [base_len:-4].replace ("\\", '/').replace ('/_', '/:'),
                        path [base_len:].replace ("\\", '/'),
                    ))
            return pathes

        def find (root, base, meta):
            if not os.path.isdir (root):
                return None, [], set ()

            routes = []
            pathes = []
            bases = set ()
            names = set ()
            for path, vue in collect (root, len (root)):
                if path.endswith ('/index'):
                    path = path [:-6] or '/'
                    full_path = (prefix + path) or '/'
                else:
                    full_path = prefix + path

                ss = []
                for s in full_path.split ('/') [1:]:
                    if not s or s [0] == ":":
                        break
                    ss.append (s)
                ss = '/' + '/'.join (ss)
                bases.add (ss)

                vue_path = "{}{}{}".format (VIEWS_DIR, base, vue)
                current_meta = {}
                for k, v in meta.items ():
                    if path.startswith (k):
                        current_meta.update (v)
                if '/components/' not in path:
                    routes.append ('{name: "%s", path: "%s", component: () => loadModule("%s", options), meta: %s}' % (path [1:] or 'index', path, vue_path, json.dumps (current_meta)))
                if path == '/':
                    context.push (vue_path) # server push
                else:
                    pathes.append (vue_path) # prefetch

            routes_list = ",\n ".join (routes)
            return routes_list, pathes, bases

        def validate_routes (bases):
            match = False
            current_uri = context.request.split_uri () [0]
            if current_uri == '/':
                match = True
            elif bases == {'/'}:
                match = True
            else:
                for base in bases:
                    if base == '/':
                        continue
                    if current_uri.startswith (base):
                        needle = current_uri [len (base):]
                        if not needle or needle [0] == '/':
                            match = True
                            break

            if not match:
                raise context.HttpError ("404 Not Found")

        global ROUTES_CACHE
        if not app.debug and base in ROUTES_CACHE:
            cached = ROUTES_CACHE [base]
            validate_routes (cached [-1])
            return cached [:3]

        if "STATIC_PATH" in app.config:
            static_path = app.config.STATIC_ROOT
        else:
            static_path = os.path.join (app.home, 'static') # default

        current = os.path.join (static_path, VIEWS_DIR [1:], base [1:])
        if not os.path.exists (current):
            raise context.HttpError ("500 Server Error", f"routes not found, `{current}` is missing")

        meta = {}
        meta_file = os.path.join (current, 'meta.json')
        if os.path.isfile (meta_file):
            with open (meta_file) as f:
                meta = json.loads (f.read ())

        routes_list, pathes, bases = find (current, base, meta)

        layout = os.path.join (VIEWS_DIR [1:], base [1:], 'layout.vue')
        assert os.path.isfile (os.path.join (static_path, layout)), f'`{ os.path.join (static_path, layout) }` is missing'

        if not routes_list:
            home = os.path.join (os.path.dirname (__file__), '../static', VIEWS_DIR [1:], base [1:])
            routes_list, pathes, bases = find (home, base, meta)

        assert routes_list, context.HttpError ("500 Server Error", 'no routes for vue-router, at least `index.vue` is expected')
        assert 'name: "index"' in routes_list, context.HttpError (f'`{ os.path.join (static_path, VIEWS_DIR [1:], base [1:], "index.vue") }` is missing')
        ROUTES_CACHE [base] = (routes_list, pathes, layout, bases)
        validate_routes (bases)
        return routes_list, pathes, layout

    # template filters --------------------------------------
    @app.template_filter ('vue')
    def vue (val):
        return '{{ %s }}' % val

    @app.template_filter ('summarize')
    def summarize (val, chars = 60):
        if not val:
            return ''
        s = val.find (" ", chars)
        if s == -1:
            return RX_SPACE.sub (" ", val.strip ())
        else:
            return RX_SPACE.sub (" ", val.strip () [: min (s, chars + 10)]) + '...'

    @app.template_filter ('attr')
    def attr (val):
        if not val:
            return '""'
        return '"{}"'.format (val.replace ('"', '\\"'))

    @app.template_filter ('upselect')
    def upselect (val, *names, **upserts):
        d = {}
        for k, v in val.items ():
            if k in names:
                d [k] = v
        d.update (upserts)
        return d

    @app.template_filter ('tojson_with_datetime')
    def tojson_with_datetime (data):
        return json.dumps (data, cls = ISODateTimeWithOffsetEncoder, ensure_ascii = False)

    @app.template_filter ('tocamel')
    def to_camel_case (text):
        s = text.replace("-", " ").replace("_", " ")
        s = s.split()
        if len(text) == 0:
            return text
        return s[0] + ''.join(i.capitalize() for i in s[1:])

    @app.template_filter ('tosnake')
    def to_snake_case (text):
        return ''.join(['_' + ch.lower() if i > 0 and ch.isupper() else ch.lower() for i, ch in enumerate(text)])



def __mount__ (context: Context, app: Atila):
    import atila_vue

    @app.route ("/ping")
    def ping (context, e: str = None):
        if e == 'v':
            raise ValueError ('emulate error')
        elif e == 'a':
            assert t != 'a', context.HttpError ('400 Bad Request', 'emulate error')
        elif e == 'h':
            raise context.HttpError ('401 Denied', 'emulate error')
        return 'pong'

    @app.route ('/base-template')
    def baee_template (context: Context):
        return f"Template Version: {atila_vue.__version__}"
