from __future__ import annotations
import time
import os
from . import handler
from atila import Context, Atila

def __mount__ (context: Context, app: Atila):
    from . import providers
    try:
        auth_handler = app.config ["AUTH_HANDLER"]
    except KeyError:
        raise RuntimeError ("app.config.AUTH_HANDLER is not defined")

    @app.get ("/onetime_token")
    @app.permission_required (['user'])
    def get_ott (context: Context, timeout: int = 1200, ott: str = None):
        if ott:
            payload = context.decode_ott (ott)
            assert payload, context.HttpError ('401 Unauthorized', 'token has been expired')
            return context.API (verified = True)

        return context.API (token = context.encode_ott (
            {'uid': context.request.user.uid, 'iat': int (time.time ())},
            timeout
        ))

    def create_token (context, payload):
        if isinstance (payload, str):
            payload = { 'email': payload, 'typ': 'rt' }
            due = auth_handler.get_refresh_token_due ()
        else:
            due = auth_handler.get_access_token_due ()

        payload ['iat'] = int (time.time ())
        payload ['exp'] = int (payload ['iat'] + due)
        if 'typ' not in payload:
            payload ['typ'] = 'at'
        return context.encode_jwt (payload)

    def check_payload (context, payload):
        assert 'err' not in payload, context.HttpError ("401 Unauthorized", payload ['err'])
        assert 'uid' in payload and 'grp' in payload and 'email' in payload, context.HttpError ("500 Internal Server Error", 'missing uid or grp')
        return payload

    @app.route ('/delete_user', methods = ['POST', 'OPTIONS'])
    @app.permission_required (["user"])
    def delete_user (context: Context):
        user = auth_handler.get_account (context.request.user.email)
        auth_handler.delete_account (user)
        return context.API ("204 No Content")

    @app.route ('/create_user_with_email_and_password', methods = ['POST', 'OPTIONS'])
    def create_user_with_email_and_password (context: Context, email: email, password: password):
        user = auth_handler.get_account (email, password)
        if user:
            raise context.HttpError ("400 Bad Request", "email-already-exists")
        auth_handler.create_account (email, password)
        return context.reroute (signin_with_email_and_password, email, password)

    @app.route ('/signin_with_email_and_password', methods = ['POST', 'OPTIONS'])
    def signin_with_email_and_password (context: Context, email: email, password: password):
        user = auth_handler.get_account (email, password)
        if not user:
            raise context.HttpError ("401 Unauthorized", "invalid credential")
        payload = check_payload (context, auth_handler.get_jwt_payload (user))
        return context.API (
            access_token = create_token (context, payload),
            refresh_token = create_token (context, payload ['email'])
        )

    @app.route ('/access_token', methods = ['POST', 'OPTIONS'])
    def access_token (context: Context, refresh_token: str):
        claim = context.decode_jwt ()
        is_valid_access_token = False
        if claim ['ecd'] not in (0, 3): # corrupted/unverified token
            raise context.HttpError ("401 Unauthorized", claim ['err'])
        if claim ['ecd'] == 0:
            if claim ['typ'] != 'at':
                raise context.HttpError ("400 Bad Request", "invalid access token")
            if claim ['exp'] > int (time.time ()) + 600:
                is_valid_access_token = True

        rt_claim = context.decode_jwt (refresh_token)
        if rt_claim ['typ'] != 'rt':
            raise context.HttpError ("400 Bad Request", "invalid refresh token")
        if 'err' in rt_claim:
            raise context.HttpError ("401 Unauthorized", rt_claim ['err'])

        payload = None
        if not is_valid_access_token:
            user = auth_handler.get_account (rt_claim ['email'])
            payload = check_payload (context, auth_handler.get_jwt_payload (user))

        return context.API (
            access_token = None if payload is None else create_token (context, payload),
            refresh_token = create_token (context, rt_claim ['email']) if rt_claim ['exp'] + 7 > time.time () else None
        )

    if "GOOGLE_APPLICATION_CREDENTIALS" in app.config:
        from firebase_admin import messaging
        from firebase_admin import auth
        from firebase_admin._auth_utils import InvalidIdTokenError

        @app.route ("/firebase/send_fcm_message", methods = ["POST", "OPTIONS"])
        def send_fcm_message (context: Context, token: str, title: str, body: str, link: str = None):
            time.sleep (2) # wait into backgound
            message = messaging.Message (
                token = token,
                data = dict (title = title, body = body, link = link)
            )
            response = messaging.send (message)
            return context.API (id = response)

        @app.route ("/firebase/signin_with_id_token", methods = ["POST", "OPTIONS"])
        def signin_with_firebase_id_token (context: Context, id_token: str, decoded_token: dict = None):
            if decoded_token is None:
                try:
                    decoded_token = auth.verify_id_token (id_token)
                except InvalidIdTokenError:
                    raise context.HttpError ("400 Bad Request", "invalid ID token")
                except Exception as e:
                    raise context.HttpError ("400 Bad Request")
            else:
                assert context.request.remote_addr == '127.0.0.1'
            # decoded_token = {'iss': 'https://securetoken.google.com/atila-vue', 'aud': 'atila-vue', 'auth_time': 1688909507, 'user_id': 'HLVNXkjGg6RZQBKza5JxjScHGUo2', 'sub': 'HLVNXkjGg6RZQBKza5JxjScHGUo2', 'iat': 1688909507, 'exp': 1688913107, 'email': 'hansroh@example.com', 'email_verified': False, 'firebase': {'identities': {'email': ['hansroh@example.com']}, 'sign_in_provider': 'password'}, 'uid': 'HLVNXkjGg6RZQBKza5JxjScHGUo2'}

            email = decoded_token ['email']
            user = auth_handler.get_account (email) or auth_handler.create_account (email)
            if decoded_token.get ('email_verified', True):
                auth_handler.set_email_verified (user)

            payload = check_payload (context, auth_handler.get_jwt_payload (user))
            return context.API (
                refresh_token = create_token (context, payload ['email']),
                access_token = create_token (context, payload)
            )

        @app.route ('/firebase/custom_token', methods = ["POST", "OPTIONS"])
        def firebase_custom_token (context: Context, provider: str, access_token: str, payload: dict = None):
            if not providers.is_valid_provider (provider):
                raise context.HttpError ("400 Unknown OAuth Provider")

            if payload is None:
                payload = providers.get_profile (provider, access_token)

            uid, profile = providers.get_uid_and_profile (provider, payload)
            return context.API (
                custom_token = auth.create_custom_token (uid).decode (),
                profile = profile,
                urls = { 'signin': context.urlspec (signin_with_firebase_id_token) }
            )
