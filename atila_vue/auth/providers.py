import hashlib
import base64
import requests

PROVIDERS = {
    'naver': 'https://openapi.naver.com/v1/nid/me',
    'kakao': 'https://kapi.kakao.com/v2/user/me'
}

def _get_uid_and_profile (provider, payload):
    def gender_code (v):
        if not v:
            return ''
        if v.lower () in ('m', 'male'):
            return 'male'
        elif v.lower () in ('f', 'female'):
            return 'female'
        return 'etc'

    profile = {}
    profile ['providerId'] = provider
    if provider == 'kakao':
        id = payload ['id']
        account = payload ['kakao_account']
        profile ['displayName'] = account ['profile'].get ('nickname', '')
        profile ['photoURL'] = account ['profile'].get ('thumbnail_image_url', '')

    elif provider == 'naver':
        account = payload ['response']
        id = account ['id']
        profile ['displayName'] = account.get ('nickname', '')
        profile ['photoURL'] = account.get ('profile_image', '')

    profile ['uid'] = id
    profile ['birthday'] = account.get ('birthday', '').replace ('-', '')
    profile ['email'] = account.get ('email', '')
    profile ['gender'] = gender_code (account.get ('gender', ''))
    profile ['name'] = account.get ('name', '')
    profile ['phoneNumber'] = ''
    profile ['emailVerified'] = True # assume all true

    # make 28 bytes UID
    uid = '{}-{}'.format (provider, id)
    uid = base64.encodestring (hashlib.md5 (uid.encode ()).digest () + b'-cust') [:-1].decode ().replace ('/', '-').replace ('+', '.')
    return uid, profile

def is_valid_provider (provider):
    return provider in PROVIDERS

def get_profile (provider, access_token):
    endpoint = PROVIDERS [provider]
    resp = requests.get (endpoint, headers = {'Authorization': 'Bearer {}'.format (access_token)})
    return resp.json ()