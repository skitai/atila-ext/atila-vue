import os
from urllib import parse
import smtplib, ssl
from email.mime.text import MIMEText
from email.utils import formataddr, formatdate, getaddresses

DEFAULT_SMTP = None

def parse_smtp (smtp):
    # SMTP=stmtps://user%40server.com:password@server.com:465
    if not smtp:
        return

    parts = parse.urlparse (os.getenv ('SMTP'))
    SMTP = {'scheme': parts.scheme}

    _login, _host = parts.netloc.split ('@')
    try:
        SMTP ['server'], _port = _host.split (":")
    except ValueError:
        SMTP ['server'], _port = _host, 25 if parts.scheme == 'smtp' else 587
    SMTP ['port'] = int (_port)

    try:
        _username, _password = _login.split (':')
    except ValueError:
        _username, _password = _login, None
    SMTP ['username'], SMTP ['password'] = parse.unquote (_username), parse.unquote (_password)
    return SMTP

def get_default_smtp ():
    return DEFAULT_SMTP

def sendable ():
    return True if DEFAULT_SMTP else False

def set_default_smtp (smtp):
    global DEFAULT_SMTP
    DEFAULT_SMTP = parse_smtp (smtp)
    return DEFAULT_SMTP

def send (sender, receivers, subject, message):
    smtp = DEFAULT_SMTP
    if isinstance (receivers, str):
        receivers = getaddresses (receivers.split (','))

    to_addrs = [ formataddr (it) for it in receivers ]
    if isinstance (message, str):
        message = MIMEText (message)

    message ['Date'] = formatdate ()
    message ['Importance'] = 'High'
    message ['From'] = sender
    message ['To'] = ', '.join (to_addrs)
    message ['Subject'] = subject

    if smtp ['scheme'] == 'smtps':
        server = smtplib.SMTP_SSL (smtp ['server'], smtp ['port'], context = ssl.create_default_context ())
    else:
        server = smtplib.SMTP (smtp ['server'], smtp ['port'])

    with server:
        if smtp ['scheme'] == 'smtptls':
            server.ehlo ()
            server.starttls ()
        server.login (smtp ['username'], smtp ['password'])
        resp = server.sendmail (
            message ['From'], to_addrs, message.as_string ()
        )
    return resp