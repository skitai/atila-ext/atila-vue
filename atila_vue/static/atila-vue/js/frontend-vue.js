const _vuexState = {}

function assign_state (name, value) {
  if (_vuexState [name] !== undefined) {
    delete _vuexState [name]
  }
  _vuexState [name] = value
}


// vuex initial state --------------------------------------
_vuexState.$uid = null
_vuexState.$grp = null
_vuexState.$claims = null
_vuexState.$g = {}
_vuexState.$authentificated = false
_vuexState.$location = location
_vuexState.$location.uri = location.pathname + (location.search || '') + (location.hash || '')
_vuexState.$ls = new CachableStorage ('local')
_vuexState.$ss = new CachableStorage ('session')
_vuexState.$idb = null
_vuexState.$urlspecs = {}
_vuexState.$waiting = false
_vuexState.$error_blocked = false
_vuexState.$websocket = {url: null, sock: null, read_handler: null, push: null}
_vuexState.$csrf = get_csrf ()
_vuexState.$next_route = null
_vuexState.$refresh_token_api = null

function mapVuexItems () {
  return Vuex.mapState ([...Object.keys (_vuexState)])
}

























// vuex data loader ----------------------------------------
function _getTargetStore (schema, props) {
  let attr_name = schema.dataset.name
  let target_object = props.store
  const variables = schema.dataset.name.split ('.')
  if (variables.length == 2) {
    target_object = props.store [variables [0]]
    attr_name = variables [1]
  } else if (variables.length > 2) {
    throw new Error ('invalid map_data name')
  }
  return [target_object, attr_name]
}

function _getDataset (css, typeref) {
  const els = document.querySelectorAll (css)
  let rows = []
  els.forEach ((el, index) => {
  let row = {}
  for (let [key, val] of Object.entries (el.dataset)) {
    const t = typeof (typeref [key])
    if (val == 'null') {
      val = null
    } else if (t == 'boolean') {
      val = (val == 'true')
    } else if (t == 'number') {
      if (val.indexOf (".") != -1) {
        val = parseFloat (val)
      } else {
        val = parseInt (val)
      }
    }
    row [key] = val
  }
  rows.push (row)
  })
  return rows
}

function _getSchemaProps (schema) {
  let store = Vuex.useStore ().state
  const type = schema.dataset.type
  const container = schema.dataset.container
  let dataSize = 0
  if (!!schema.dataset.maxSize) {
    dataSize = parseInt (schema.dataset.maxSize)
  }
  return {type, store, dataSize, container}
}

function _readSchemas () {
  const schemas = document.querySelectorAll ('#state-map > .veux-state')
  schemas.forEach (schema => {
  props = _getSchemaProps (schema)
  let [target_object, attr_name] = _getTargetStore (schema, props)
  if (props.type == undefined) {
    let d = []
    for (let i = 0; i < props.dataSize; i++ ) {
      d.push (JSON.parse (schema.dataset.default))
    }
    target_object [attr_name] = d
  }
  else {
    target_object [attr_name] = ''
  }
  })
}

function _readDataset () {
  const schemas = document.querySelectorAll ('#state-map > .veux-state')
  for (let i=0; i<schemas.length; i++) {
    let schema = schemas [i]
    props = _getSchemaProps (schema)

    let [target_object, attr_name] = _getTargetStore (schema, props)
    let typeref = target_object [attr_name]
    if (props.dataSize) {
      typeref = target_object [attr_name][0]
    }

    const container = document.querySelector (props.container)
    if (!!container) {
      if (props.type === 'html' && !!props.container) {
        target_object [attr_name] = container.innerHTML
      }
      else if (props.type === 'text' && !!props.container) {
        target_object [attr_name] = container.innerText
      }
      else {
        r = _getDataset (props.container, typeref)
        for (let i = 0; i < r.length; i++) {
          target_object [attr_name].splice (i, 1, r [i])
        }
      }
    }
  }
}


























const _permission = {
  granted: function () {
    const next_route = store.state.$next_route || { name: 'index' }
    store.state.$next_route = null
    router.push (next_route)
  },

  check: async function (permission) {
    while (!store.state.$authentificated) {
      await _utils.sleep (100)
      _utils.log ('waiting athentification complete...', 'debug')
    }

    if (store.state.$uid == null) {
      return false
    }
    if (permission == undefined || permission == null || permission.length == 0) {
      return true
    }
    if (store.state.$grp === null) {
      store.state.$grp = ['user']
    }

    if (store.state.$grp.indexOf ('admin') != -1) {
      return true
    }
    if (store.state.$grp.indexOf ('staff') != -1 && permission.indexOf ('staff') != -1) {
      return true
    }
    for (const perm  of store.state.$grp) {
      if (permission.indexOf (perm) != 1) {
        return true
      }
    }
    return false
  },

  required: async function (permission, redirect, routing) {
    const granted = await this.check (permission)
    if (granted) {
      return routing.next ()
    }
    store.state.$next_route = {name: routing.to.name}
    return routing.next (redirect)
  },

  unauthentification_required: async function (redirect, routing) {
    const granted = await this.check ()
    if (granted) {
      return routing.next (redirect)
    }
    routing.next ()
  },
}























const _vue_utils = {
  permission: _permission,

  set_cloak: function (flag) {
    store.state.$cloak = flag
  },

  wait_for_credential: async function () {
    await frontend.wait_for_true ('state.$has_credential', 10000)
  },

  setup_sentry: async function () {
    state = store.state
    if (!window.Sentry) {
      return
    }
    Sentry.configureScope ((scope) => {
      scope.setTag ('context', 'frontend')
      scope.setUser ({
        uid: state.$uid || null,
        email: state.$claims?.email || null
      })
    })
  },

  error_captured: function (err, vm, info) {
    const context = {
      scope: info,
      component: `${vm.$options.__name}.vue`,
      baseURI: !!vm.$el ? vm.$el.baseURI : null,
    }

    for (const m of document.querySelectorAll ('.modal')) {
      modal = bootstrap.Modal.getInstance (m)
      if (!!modal) {
        modal.hide ()
      }
    }

    this.traceback (err, {context, tags: {'capturedBy': 'vue.errorHandler'}})
  },

  traceback: function (e, options = {}) {
    store.state.$waiting = false
    let context = this.build_error_context (e)
    if (!!options.context && typeof (options.context) == 'object') {
      context = {...context, ...options.context}
    }

    let tags = {system: 'frontend'}
    if (!!options.tags && typeof (options.tags) == 'object') {
      tags = {...tags, ...options.tags}
    } else {
      tags.capturedBy = 'traceback'
    }

    if (!!context.component) {
      context.text += `|${context.scope}|${context.component}`
    }
    const info = `${e.name} ${context.text}`
    this.log (info, 'expt')
    if (__debug__) {
      store.state.$traceback = info
    }
    if (!!context.traceback) {
      const info = JSON.stringify (context.traceback, null, 2)
      this.log (info, 'debug')
      if (__debug__) {
        store.state.$traceback += '\n\n' + info
      }
    }
    if (!!window.Sentry) {
      Sentry.withScope ((scope) => {
        scope.setContext ('context', context)
        for (const k in tags) {
          scope.setTag (k, tags [k])
        }
        e.message = context.text
        Sentry.captureException (e)
      })
    }
    return context.text
  }
}





















for (const key in _vue_utils) {
  window.frontend [key] =_vue_utils [key]
}
