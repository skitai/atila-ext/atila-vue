const _device = {
  android: function() {
    return navigator.userAgent.match(/Android/i)
  },
  ios: function() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i)
  },
  mobile: function() {
    return (deviceDetect.android() || deviceDetect.ios())
  },
  touchable: function () {
    return (navigator.maxTouchPoints || 'ontouchstart' in document.documentElement)
  },
  rotatable: function () {
    return window.orientation > -1
  },
  width: function () {
    return window.innerWidth
  },
  height: function () {
    return window.innerHeight
  },
  breakpoint: function () {
    const w = this.width ()
    if ('bootstrap' in window) {
      if (w < 576) return {name: 'xs', index: 0}
      if (w < 768) return {name: 'sm', index: 1}
      if (w < 992) return {name: 'md', index: 2}
      if (w < 1200) return {name: 'lg', index: 3}
      if (w < 1400) return {name: 'xl', index: 4}
      return {name: 'xxl', index: 5}
    }
    return {name: 'na', index: -1}
  }
}














class BaseValidationError extends Error {
  constructor (key, attr, op, desired, op_help, name) {
    super ()
    this.error = true
    this.key = key
    this.status_code = 901
    this.status = 'Validation Error'

    this.message = this.generateMessage (key, attr, op, desired, op_help, name)
    this.data = {
      code: 90100,
      message: "validation error",
      more_info: this.message,
      vaildationInfo: {key: key, attr: attr, op: op, desired: desired, op_help: op_help, name: name}
    }
    this.errorText = `${this.message} (status: ${this.data.message}, error: ${this.data.code})`
    this.name = "ValidationError"
  }

  generateMessage (key, attr, op, desired, op_help, name) {
    const disp_name = name || key.replace ('_', ' ')
    let s = ''
    if (attr == 'length') {
      s = `The length of ${disp_name}`
    } else if (attr == 'type') {
      s = `The type of ${disp_name}`
    } else {
      s = disp_name [0].toUpperCase() + disp_name.slice(1).toLowerCase()
    }
    return `${s} ${op_help}`
  }
}

const OPS = {
  lte: (key, val, attr, cond, desired, name) => val <= desired ? null : _valid_failed (key, attr, cond, desired, `should be less than or equal to ${desired}`, name),
  lt: (key, val, attr, cond, desired, name) => val < desired ? null : _valid_failed (key, attr, cond, desired, `should be less than ${desired}`, name),
  gte: (key, val, attr, cond, desired, name) => val >= desired ? null : _valid_failed (key, attr, cond, desired, `should be greater than or equal to ${desired}`, name),
  gt: (key, val, attr, cond, desired, name) => val > desired ? null : _valid_failed (key, attr, cond, desired, `should be greater than  ${desired}`, name),

  between: (key, val, attr, cond, desired, name) => (val >= desired [0] && val <= desired [1]) ? null : _valid_failed (key, attr, cond, desired, `should be between ${desired [0]} and ${desired [1]}`, name),
  in: (key, val, attr, cond, desired, name) => desired.indexOf (val) != -1 ? null : _valid_failed (key, attr, cond, desired, `should contain ${desired}`, name),
  notin: (key, val, attr, cond, desired, name) => desired.indexOf (val) == -1 ? null : _valid_failed (key, attr, cond, desired, `should not contain ${desired}`, name),

  codein: (key, val, attr, cond, desired, name) => desired.find((element) => element [0] === val) ? null : _valid_failed (key, attr, cond, desired, `should be one of ${desired}`, name),
  codenotin: (key, val, attr, cond, desired, name) => desired.find((element) => element [0] === val) == undefined ? null : _valid_failed (key, attr, cond, desired, `should be none of ${desired}`, name),

  eq: (key, val, attr, cond, desired, name) => val == desired ? null : _valid_failed (key, attr, cond, desired, `should be ${desired}`, name),
  ieq: (key, val, attr, cond, desired, name) => val.toLowerCase () == desired ? null : _valid_failed (key, attr, cond, desired, `should be ${desired}`, name),
  neq: (key, val, attr, cond, desired, name) => val != desired ? null : _valid_failed (key, attr, cond, desired, `should not be ${desired}`, name),
  ineq: (key, val, attr, cond, desired, name) => val.toLowerCase () != desired ? null : _valid_failed (key, attr, cond, desired, `should not be ${desired}`, name),

  contains: (key, val, attr, cond, desired, name) => desired.indexOf (val) != -1 ? null : _valid_failed (key, attr, cond, desired, `should contain ${desired}`, name),
  ncontains: (key, val, attr, cond, desired, name) => desired.indexOf (val) == -1 ? null : _valid_failed (key, attr, cond, desired, `should not contain ${desired}`, name),
  icontains: (key, val, attr, cond, desired, name) => val.indexOf (desired.toLowerCase ()) != -1 ? null : _valid_failed (key, attr, cond, desired, `should contain ${desired}`, name),
  incontains: (key, val, attr, cond, desired, name) => val.indexOf (desired.toLowerCase ()) == -1 ? null : _valid_failed (key, attr, cond, desired, `should not contain ${desired}`, name),

  startswith: (key, val, attr, cond, desired, name) => val.startsWith (desired) ? null : _valid_failed (key, attr, cond, desired, `should start with ${desired}`, name),
  istartswith: (key, val, attr, cond, desired, name) => val.startsWith (desired.toLowerCase ()) ? null : _valid_failed (key, attr, cond, desired, `should start with ${desired}`, name),
  nstartswith: (key, val, attr, cond, desired, name) => !val.startsWith (desired) ? null : _valid_failed (key, attr, cond, desired, `should not start with ${desired}`, name),
  instartswith: (key, val, attr, cond, desired, name) => !val.startsWith (desired.toLowerCase ()) ? null : _valid_failed (key, attr, cond, desired, `should not start with ${desired}`, name),

  endswith: (key, val, attr, cond, desired, name) => val.endsWith (desired) ? null : _valid_failed (key, attr, cond, desired, `should end with ${desired}`, name),
  iendswith: (key, val, attr, cond, desired, name) => val.endsWith (desired.toLowerCase ()) ? null : _valid_failed (key, attr, cond, desired, `should end with ${desired}`, name),
  nendswith: (key, val, attr, cond, desired, name) => !val.endsWith (desired) ? null : _valid_failed (key, attr, cond, desired, `should not end with ${desired}`, name),
  inendswith: (key, val, attr, cond, desired, name) => !val.endsWith (desired.toLowerCase ()) ? null : _valid_failed (key, attr, cond, desired, `should not end with ${desired}`, name),

  regex: (key, val, attr, cond, desired, name) => new RegExp(`${desired}`, 'g').test (val) ? null : _valid_failed (key, attr, cond, desired, `is not valid`, name),
  iregex: (key, val, attr, cond, desired, name) => new RegExp(`${desired}`, 'gi').test (val) ? null : _valid_failed (key, attr, cond, desired, `is not valid`, name),

  nregex: (key, val, attr, cond, desired, name) => new RegExp(`${desired}`, 'g').test (val) == false ? null : _valid_failed (key, attr, cond, desired, `is not valid`, name),
  inregex: (key, val, attr, cond, desired, name) => new RegExp(`${desired}`, 'gi').test (val) == false ? null : _valid_failed (key, attr, cond, desired, `is not valid`, name),
}

OPS.range = OPS.between
OPS.exact = OPS.eq
OPS.iexact = OPS.ieq
OPS.nexact = OPS.neq
OPS.inexact = OPS.ineq

function _valid_failed (key, attr, op, desired, op_help, name) {
  try {
    return new ValidationError (key, attr, op, desired, op_help, name)
  } catch (e) {
    if (e.name == 'ReferenceError') {
      return new BaseValidationError (key, attr, op, desired, op_help, name)
    } else {
      throw e
    }
  }
}

const _validator = {
  email: {required: true, len__lte: 64, iregex: '^[a-z0-9][-.a-z0-9]*@[-a-z0-9]+\\.[-.a-z0-9]{2,}[a-z]$'},
  password: {required: true, len__lte: 64, password: true},

  show_feedback (key, attr, op_help, name) {
    let err = null
    if (typeof (key) == 'object') {
      err = key
    } else {
      err = _valid_failed (key, attr, null, null, op_help, name)
    }

    const c = document.querySelector (`.bs-input #input-${err.key}`)
    if (c == null) {
      _utils.log (`cannot find '.bs-input #input-${err.key}', rethrowing error`, 'warn')
      throw err
    }

    const errorDiv = document.createElement ('div')
    errorDiv.setAttribute ("class", "input-vaild-feedback")
    errorDiv.innerHTML = '⭕ ' + err.message
    c.parentElement.appendChild (errorDiv)
    if (c.parentElement.classList.contains ('bs-input')) {
      c.parentElement.parentElement.classList.add ('input-invaild-display')
    } else {
      c.parentElement.classList.add ('input-invaild-display')
    }
    c.focus ()
  },

  hide_feedback () {
    const c = document.querySelector (`.input-vaild-feedback`)
    if (!!c) {
      if (c.parentElement.classList.contains ('bs-input')) {
        c.parentElement.parentElement.classList.remove ('input-invaild-display')
      } else {
        c.parentElement.classList.remove ('input-invaild-display')
      }

      c.remove ()
    }
  },

  failed (params, validators) {
    this.hide_feedback ()
    try {
      this.validate (params, validators)
    } catch (e) {
      if (e.status_code == 901) {
        this.show_feedback (e)
        return true
      } else {
        throw e
      }
    }
    return false
  },

  validate (params, validators) {
    if (!!validators.length) return

    for (let key in params) { // check invalid params
      if (!(key in validators)) {
        throw _valid_failed (key, 'value', 'is', 'unknown', `is not a valid parameter`)
      }
      if (!!validators [key].protected) {
        throw _valid_failed (key, 'value', 'is', 'protected', `is not editable`, validators [key].__name__)
      }
    }

    for (let key in validators) { // check missing param
      const val = params [key]
      if (!!validators [key].protected) {
        continue
      }
      if (!!validators [key].required && (val === undefined || val === null || val === '')) {
        throw _valid_failed (key, 'value', 'is', 'required', `is required`, validators [key].__name__)
      }
    }

    for (let key in params) {
      conditions = []
      const validator = validators [key]
      let val = params [key]

      if (!!!validator) {
        continue
      }

      if (!!validator.password) { // for detail hont message, it doesn't use nregex
        if (/^(.{0,7}|[^0-9]*|[a-zA-Z0-9]*)$/g.test (val)) {
          throw _valid_failed (key, 'value', 'is', 'password', `should contain at least one digit and special character`, validator.__name__)
        }
      }

      if (!!validator.type) {
        const desired = validator.type
        let newVal = val

        if (val == null) continue

        if (desired == 'email') {
          if (!/^[a-z0-9][-.a-z0-9]*@[-a-z0-9]+\.[-.a-z0-9]{1,}[a-z]$/gi.test (val)) {
            throw _valid_failed (key, 'format', 'is', desired, `should be ${desired} format`, validator.__name__)
          }
        }
        else if (desired == 'int') {
          newVal = parseInt (val)
          if (Object.is (newVal, NaN)) {
            throw _valid_failed (key, 'type', 'is', desired, `should be ${desired}`, validator.__name__)
          }
        }
        else if (desired == 'float') {
          newVal = parseFloat (val)
          if (Object.is (newVal, NaN)) {
            throw _valid_failed (key, 'type', 'is', desired, `should be ${desired}`, validator.__name__)
          }
        }
        else if (desired == 'bool') {
          if (val === true || val === 'true' || val === 'yes') {
            newVal = true
          } else if (val === false || val === 'false' || val === 'no') {
            newVal = false
          } else {
            throw _valid_failed (key, 'type', 'is', desired, `should be ${desired}`, validator.__name__)
          }
        } else if (desired == 'file') {
            if (val.constructor !=  File && val.constructor != Blob) {
              throw _valid_failed (key, 'type', 'is', desired, `should be ${desired}`, validator.__name__)
            }
        } else if (desired == 'json') {
          try {
            JSON.parse (val)
          } catch {
            throw _valid_failed (key, 'type', 'is', desired, `should be ${desired}`, validator.__name__)
          }
        }
        params [key] = newVal
        val = newVal
      }

      for (let cond in validator) {
        if (cond.startsWith ('__')) {
          continue
        }
        let attr = 'value'
        let curVal = val
        const desired = validator [cond]
        if (cond.indexOf ('len__') == 0) {
          cond = cond.substring (5, cond.length)
          attr = 'length'
          curVal = val.length
        }

        if (!(cond in OPS)) {
          continue
        }
        const error = OPS [cond] (key, curVal, attr, cond, desired, validator.__name__)
        if (error != null) {
          throw error
        }
      }
    }

    for (let key in params) { // prefer null than blank
      if (params [key] === '') {
        params [key] = null
      }
    }
  },
}



















const _layout = {
  _get_base_width: function (parent) {
    if (!!parent) {
      const p = document.querySelector (parent)
      return (!!p ? p.offsetWidth : 0)
    }
    return window.innerWidth
  },

  _get_base_height: function (parent) {
    if (!!parent) {
      const p = document.querySelector (parent)
      return (!!p ? p.offsetHeight : 0)
    }
    return window.innerHeight
  },

  wait_for_exist: async function (css, timeout = 1000) {
    let obj = null
    const max_retry = parseInt (timeout / 100)
    for (let i = 0; i < max_retry; i++) {
      obj = document.querySelector (css)
      if (obj != null) {
        return obj
      }
      await frontend.sleep (100)
    }
  },

  wait_for_visible: async function (css, timeout = 1000) {
    let obj = null
    const max_retry = parseInt (timeout / 100)
    for (let i = 0; i < max_retry; i++) {
      obj = document.querySelector (css)
      if (obj != null && obj.offsetWidth) {
        return obj
      }
      await frontend.sleep (100)
    }
  },

  wait_for_not_exist: async function (css, timeout = 1000) {
    let obj = null
    const max_retry = parseInt (timeout / 100)
    for (let i = 0; i < max_retry; i++) {
      obj = document.querySelector (css)
      if (obj == null) {
        return
      }
      await frontend.sleep (100)
    }
  },

  is_visible: function (domElement) {
    return new Promise(resolve => {
      const o = new IntersectionObserver(([entry]) => {
        resolve(entry.intersectionRatio === 1)
        o.disconnect()
      })
      o.observe(domElement)
    })
  },

  scroll_into_view: async function (css, adjust = 0) {
    const el = await this.wait_for_visible (css)
    if (!el) {
      return
    }
    window.scrollTo ({top: el.offsetTop + adjust})
  },

  keep_bottom_margin: async function (css, bottom_margin = 0, initial_delay = 0, parent = null) {
    function fixed (event) {
      const baseHeight = _layout._get_base_height (parent)
      if (!baseHeight) return
      const obj = document.querySelector (css)
      obj.style.height = (baseHeight - obj.offsetTop - bottom_margin) + 'px'
    }
    addEventListener("resize", (event) => {fixed (event)})
    await _utils.sleep (initial_delay)
    fixed ()
  },

  keep_right_margin: async function (css, right_margin = 0, initial_delay = 0, parent = null) {
    function fixed (event) {
      const baseWidth = _layout._get_base_width (parent)
      if (!baseWidth) return
      const obj = document.querySelector (css)
      if (baseWidth > obj.offsetLeft + obj.offsetWidth) {
        obj.style.width = (baseWidth - obj.offsetLeft - right_margin) + 'px'
      }
    }
    addEventListener("resize", (event) => {fixed (event)})
    await _utils.sleep (initial_delay)
    fixed ()
  },

  keep_fill_height: async function (css, bottom_margin = 0, initial_delay = 0, parent = null) {
    function fixed (event) {
      const baseHeight = _layout._get_base_height (parent)
      if (!baseHeight) return
      const obj = document.querySelector (css)
      const new_height = (baseHeight - obj.offsetTop - bottom_margin)
      obj.style.minHeight = new_height + 'px'
    }
    addEventListener("resize", (event) => {fixed (event)})
    await _utils.sleep (initial_delay)
    fixed ()
  },

  keep_relative_width: async function (css, end_offset, parent = null) {
    function fixed (event) {
      const baseWidth = _layout._get_base_width (parent)
      if (!baseWidth) return
      for (const it of document.querySelectorAll (css)) {
        it.style.maxWidth = (baseWidth + end_offset) + 'px'
        it.style.width = (baseWidth + end_offset) + 'px'
      }
    }
    while (true) {
      const span = document.querySelector (css)
      if (span != null) {
        fixed ()
        addEventListener ("resize", (event) => {fixed (event)})
        // document.querySelector (parent).addEventListener ("resize", (event) => {fixed (event)})
        break
      }
      await _utils.sleep (1000)
    }
  },

  is_scroll_x_start: function (ele, predetect = 0) {
    return (ele.scrollLeft <= predetect)
  },

  is_scroll_x_end: function (ele, predetect = 0) {
      return (Math.ceil(ele.scrollLeft + ele.offsetWidth) >= ele.scrollWidth - predetect)
  },

  is_scroll_y_start: function (ele, predetect = 0) {
      return (ele.scrollTop <= predetect);
  },

  is_scroll_y_end: function (ele, predetect = 0) {
      return (Math.ceil(ele.scrollTop + ele.offsetHeight) >= ele.scrollHeight - predetect)
  },
}



























// Aliases
const _utils = {
  device: _device,
  validator: _validator,
  layout: _layout,
  analytics: null,

  log: function (msg, type = 'info') {
    if (__debug__) {
      console.log (`[${type}] ${msg}`)
    }
  },

  build_error_context: function (e) {
    let context = {}
    if (e.response !== undefined) {
      const r = e.response
      context = r.data
      context.url = e.request.url
      context.code = r.data.code || 70000
      context.message = r.data.message || 'no message'
      context.text = `${r.status} ${r.statusText}|${context.message}|${context.code})`
    }
    else {
      context.text = `${e.message}`
    }
    return context
  },

  decode_jwt: function (token) {
    try {
      return JSON.parse (atob (token.split ('.') [1]))
    } catch (e) {
      return null
    }
  },

  load_resources: function (ns, src, callback = () => {}) {
    if (ns in window) {
      return callback ()
    }
    this.load_script (src, callback)
  },

  load_script: function (src, callback = () => {}, ns = null) {
    this.log (`loading script: ${src}`, 'debug')
    let current = null
    if (typeof (src) === "string") {
      current = src
      src = []
    } else {
      current = src.shift ()
    }

    let element = null
    if (current.toLowerCase ().endsWith (".css")) {
      element = document.createElement('link')
      element.setAttribute('href', current)
      element.setAttribute('rel', "stylesheet")
    }
    else {
      element = document.createElement('script')
      element.setAttribute('src', current)
      element.setAttribute('async', true)
    }

    if (src.length) {
      element.addEventListener('load', () => { this.load_script (src, callback) })
    } else {
      element.addEventListener('load', callback)
    }
    document.head.appendChild(element)
  },

  sleep: function (ms) {
    return new Promise (resolve => setTimeout(resolve, ms))
  },

  randrange: function (n) {
    return Math.floor(Math.random() * n)
  },

  choice: function (arr) {
    return arr [this.randrange (arr.length)]
  },

  build_url: function (baseurl, params = {}) {
    let url = baseurl
    let newquery = ''
    for (let [k, v] of Object.entries (params)) {
      if (v === null) {
        continue
      }
    if (!!newquery) {
      newquery += '&'
    }
    newquery += k + "=" + encodeURIComponent (v)
    }
    if (!!newquery) {
      return url + "?" + newquery
    }
    return url
  },

  wait_for_refval: async function (obj, interval = 30, retry = 100) {
    for (let i = 0; i < retry; i++) {
      if (!!obj.value) {
        if (typeof (obj.value) == 'object') {
          if (Object.keys (obj.value).length) {
            return true
          }
        } else {
          return true
        }
      }
      await this.sleep (interval)
    }
  },

  wait_for_define: async function (obj, interval = 30, retry = 1000) {
    if (obj.startsWith ('state.')) {
      obj = 'store.' + obj
    }
    for (let i = 0; i < retry; i++) {
      const val = eval (obj)
      if (val !== undefined) return val
      await this.sleep (interval)
      this.log (`waiting ${obj}...`)
    }
  },

  wait_for_true: async function (obj, interval = 30, retry = 1000) {
    if (obj.startsWith ('state.')) {
      obj = 'store.' + obj
    }
    for (let i = 0; i < retry; i++) {
      const val = eval (obj)
      if (!!val) return val
      await this.sleep (interval)
      this.log (`waiting ${obj}...`)
    }
  },

  ask_push_permission: async function (callback) {
    let permission = this.get_push_permission ()
    if (permission === "granted") {
      this.log ("notification granted")
      return permission
    }
    if (permission === "denied") {
      this.log ("notification blocked")
      return permission
    }

    permission = await Notification.requestPermission ()
    Notification.permission = permission
    if (permission == 'granted' && callback) {
      callback ()
    }
    return permission
  },

  get_push_permission: function () {
    if (!("Notification" in window)) {
      return 'denied'
    }
    return Notification.permission
  },

  push_alarm: async function (title, message, link = null, icon = null, timeout = 10000) {
    if (Notification.permission === "denied") {
      this.log ("notification blocked")
      return
    }
    else if (Notification.permission !== "granted") {
      const permission = await this.ask_push_permission ()
      if (permission != 'granted') {
        return
      }
    }

    const options = {body: message}
    if (!!icon) options.icon = icon
    const n = new Notification (title, options)
    n.onclick = (event) => {
      if (!!link) {
        window.open (link)
      }
      else n.close ()
    }
    n.onshow = (event) => {
      setTimeout (function(){ n.close () }, timeout)
    }
  },

  gtag: async function (event = 'page_view', params = {}) {
    if (!firebase.optional.analystic) {
      frontend.log (`ignored gtag ${location.href} ${event}`, 'debug')
      return
    }
    // frontend.log (`gtag ${location.href} ${event}`, 'debug')
    if (frontend.analytics === null) {
      frontend.analytics = await firebase.getAnalytics ()
    }
    firebase.analytics.logEvent (frontend.analytics, params)
  }
}

window.frontend = _utils
