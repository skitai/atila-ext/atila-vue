Number.prototype.format = function (type) {
  if (type == 'usd') {
    const usd = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
    }).format (this)
    return usd.substring (1, 100)
  }

  if(this == 0) return "0"
  var reg = /(^[+-]?\d+)(\d{3})/
  var n = (this + '')
  while (reg.test(n)) n = n.replace (reg, '$1' + ',' + '$2')
    return n
}

String.prototype.numberformat = function (fmt) {
  var num = parseFloat (this)
  if( isNaN(num) ) return "0"
  return num.format (fmt)
}

String.prototype.toCamelCase = function  (str) {
  return this.toLowerCase ().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase())
}


String.prototype.dateformat = function (fmt) {
  return new Date (this).format (fmt)
}

String.prototype.titleCase = function () {
  return this.replace (/\w\S*/g, function (txt) {return txt.charAt(0).toUpperCase () + txt.substr (1).toLowerCase ();})
}
Date.prototype.unixepoch = function () {
  return Math.floor(this.getTime() / 1000)
}

Date.prototype.format = function(f) {
  if (!this.valueOf()) return " "
  var d = this;
  return f.replace(/(%Y|%y|%m|%d|%H|%I|%M|%S|%p|%a|%A|%b|%B|%w|%c|%x|%X|%k|%n|%D)/gi, function($1) {
    switch ($1) {
    case "%Y":
      return d.getFullYear()
    case "%y":
      return (d.getFullYear() % 1000).zfill(2)
    case "%m":
      return (d.getMonth() + 1).zfill(2)
    case "%d":
      return d.getDate().zfill(2);
    case "%H":
      return d.getHours().zfill(2)
    case "%I":
      return ((h = d.getHours() % 12) ? h : 12).zfill(2)
    case "%M":
      return d.getMinutes().zfill(2)
    case "%S":
      return d.getSeconds().zfill(2)
    case "%p":
      return d.getHours() < 12 ? "AM" : "PM"
    case "%w":
      return d.getDay()
    case "%c":
      return d.toLocaleString()
    case "%x":
      return d.toLocaleDateString()
    case "%X":
      return d.toLocaleTimeString()
    case "%b":
      return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][d.getMonth()]
    case "%B":
      return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][d.getMonth()]
    case "%a":
      return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][d.getDay()]
    case "%A":
      return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][d.getDay()]
    case "%k":
      return ['일', '월', '화', '수', '목', '금', '토'][d.getDay()]
    case "%n":
      return ( d.getMonth() + 1)
    case "%D":
      return d.getDate()
    default:
      return $1
    }
  })
}
String.prototype.repeat = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;}
String.prototype.zfill = function(len){return "0".repeat(len - this.length) + this}
Number.prototype.zfill = function(len){return this.toString().zfill(len)}















const IGNORABLE_VUE_WARNINGS = []
function ignore_warning (str) {
  IGNORABLE_VUE_WARNINGS.push (str)
}




class CachableStorage {
  constructor (loc) {
    if (loc == 'session') {
      this.__s = window.sessionStorage
    } else {
      this.__s = window.localStorage
    }
  }
  now () {
    return Math.floor(new Date().getTime() / 1000)
  }
  set (name, data, timeout = 0) {
    this.__s.setItem (name, JSON.stringify ([ timeout ? this.now () + timeout : 0, data ]))
  }
  get (name, d = null) {
    const _val = this.__s.getItem (name)
    if (_val == null) {
      return d
    }
    const _cached = JSON.parse (_val)
    if (_cached [0] && _cached [0] < this.now ()) {
      this.remove (name)
      return d
    }
    return _cached [1]
  }
  remove (name) {
    this.__s.removeItem (name)
  }
  clear () {
    this.__s.clear ()
  }
}



class IndexedDB {
  constructor (dbName, version = 1) {
    this.dbName = dbName
    this.version = version
    this.db = null
  }

  async drop () {
    await idb.deleteDB (this.dbName)
  }

  async initialize (tableNames) {
    this.db = await idb.openDB (this.dbName, this.version, {
      upgrade(db) {
        for (const tableName of tableNames) {
          if (!db.objectStoreNames.contains (tableName)) {
            db.createObjectStore (tableName)
          }
        }
        for (const tableName of db.objectStoreNames) {
          if (tableNames.indexOf (tableName) == -1) {
            db.deleteObjectStore (tableName)
          }
        }
      },
    })
  }

  async reinitialize () { // to vaccum disk space
    const tableNames = []
    for (const tableName of this.db.objectStoreNames) {
      tableNames.push (tableName)
    }
    this.db.close ()
    await idb.deleteDB (this.dbName)
    await this.initialize (tableNames)
  }

  async transaction (tableName, mode = 'readonly') {
    while (this.db == null) {
      await frontend.sleep (80)
    }
    return this.db.transaction(tableName, mode)
  }

  async cursor (tableName) {
    const tx = await this.transaction (tableName, 'readonly')
    const store = tx.objectStore(tableName)
    return await store.openCursor ()
  }

  async *items (tableName) {
    let cursor = await this.cursor (tableName)
    while (cursor) {
      yield [cursor.key, cursor.value]
      cursor = await cursor.continue ()
    }
  }

  async *values (tableName) {
    let cursor = await this.cursor (tableName)
    while (cursor) {
      yield cursor.value
      cursor = await cursor.continue ()
    }
  }

  async *keys (tableName) {
    let cursor = await this.cursor (tableName)
    while (cursor) {
      yield cursor.key
      cursor = await cursor.continue ()
    }
  }

  async *setAll (tableName, data, batch_size = 8) {
    const tx = await this.transaction (tableName, 'readwrite')
    const store = tx.objectStore (tableName)
    const batch = []
    for (let [key, value] of data) {
      batch.push (store.put (value, key))
      if (batch.length == batch_size) {
        await Promise.all (batch)
        yield batch.length
        batch.splice (0, batch.length)
      }
    }
    if (batch.length) {
      await Promise.all (batch)
    }
    await tx.done
    yield batch.length
  }

  async getAll (tableName) {
    return await this.db.getAll (tableName)
  }

  async getAllKeys (tableName) {
    return await this.db.getAllKeys (tableName)
  }

  async count (tableName) {
    return await this.db.count (tableName)
  }

  async get (tableName, key) {
    return await this.db.get(tableName, key)
  }

  async set (tableName, key, value) {
    return await this.db.put (tableName, value, key)
  }

  async remove (tableName, key) {
    return await this.db.delete(tableName, key)
  }

  async clear (tableName) {
    await this.db.clear(tableName)
  }
}



function unint8array (decoded) {
  let arr = []
  for (let i = 0; i < decoded.length; i++) {
    arr.push (decoded.charCodeAt(i))
  }
  return new Uint8Array (arr)
}

function bytes2string (bytes) {
  let decoded = atob (bytes)
  if (decoded.charCodeAt(0) == 88) {
    decoded = decoded.substring (5, decoded.length - 3)
    return new TextDecoder().decode (unint8array (decoded))
  }
  else if (decoded.charCodeAt(0) == 78) { // None
    return null
  }
  else if (decoded.charCodeAt(0) == 77) { // short int
    decoded = decoded.substring (1, 3)
    return new Int16Array (unint8array (decoded).buffer) [0]
  }
  else if (decoded.charCodeAt(0) == 75) { // char int
    decoded = decoded.substring (1, 2)
    return new Int8Array (unint8array (decoded).buffer) [0]
  }
  else if (decoded.charCodeAt(0) == 74) { // int
    decoded = decoded.substring (1, 5)
    return new Int32Array (unint8array (decoded).buffer) [0]
  }
  throw new Error ('Unknown pickle type')
}

function parse_session () {
  let session = null
  for (let each of document.cookie.split ("; ")) {
    if (each.indexOf ('ATLSES_STK=') == 0) {
      session = each
      break
    }
  }
  if (!session) {
    return null
  }
  const user = {}
  const sessionval = session.split ('?')
  if (sessionval.length == 1) {
    return null
  }
  for (let each of sessionval [1].split ('&')) {
    const [name, val] = each.split ('=')
    if (name == 'nick_name' || name == 'uid' || name == 'lev' || name == 'status') {
      user [name] = bytes2string (val)
    }
  }
  return user
}

function get_csrf () {
  const meta = document.querySelector ('head > meta[name=csrf]')
  if (meta == null) {
    return null
  }
  const [token, name] = meta.getAttribute ('content').split (';')
  return {token, name}
}

function prefetch (href) {
  const s = document.createElement('link')
  s.rel = 'prefetch'
  s.as = 'fetch'
  s.href = href
  document.body.appendChild (s)
}

function decode_jwt (token) {
  try {
    return JSON.parse (atob (token.split ('.') [1]))
  } catch (e) {
    frontend.traceback (e)
    return null
  }
}
























const swsync = {
  periodic_sync_enabled: async function () {
    const status = await navigator.permissions.query({
      name: 'periodic-background-sync',
    })
    return status.state === 'granted'
  },

  register_tag: async function (tag, min_interval_sec = 0) {
    if (!('serviceWorker' in navigator && 'SyncManager' in window)) return false
    const registration = await navigator.serviceWorker.ready
    if (min_interval_sec) {
      const enabled = await this.periodic_sync_enabled ()
      if (!enabled) return false
      await registration.periodicSync.register (tag, {minInterval: min_interval_sec * 1000})
    } else {
      await registration.sync.register (tag)
    }
    return true
  },

  unregister_tag: async function (tag) {
    if (!('serviceWorker' in navigator && 'SyncManager' in window)) return false
    const enabled = await this.periodic_sync_enabled ()
    if (!enabled) return
    const registration = await navigator.serviceWorker.ready
    await registration.periodicSync.unregister(tag)
  },

  is_tag_registered: async function (tag) {
    if (!('serviceWorker' in navigator && 'SyncManager' in window)) return false
    const enabled = await this.periodic_sync_enabled ()
    if (!enabled) return
    const registration = await navigator.serviceWorker.ready
    const tags = await registration.periodicSync.getTags()
    return tags.includes(tag)
  },
}

window.swsync = swsync

























// default firebase namespace ----------------------------------------
const firebase = {
  optional: {
    analystic: false, messaging: false
  }
}
window.firebase = firebase
