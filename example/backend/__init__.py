import skitai
import os
from atila_vue import auth
from atila_vue.auth import handler

class MyAuthHandler (handler.ExampleAuthHandler):
    pass


def __config__ (pref):
    pref.secret_key = "DstAin8IMr2v1IbWguirp3UOQ6g8qa2TmsYfEm2VVJdRgkg=="
    pref.config.MAX_UPLOAD_SIZE = 1 * 1024 * 1024 * 1024
    pref.set_static ('/', skitai.joinpath ('backend/static'))

    cred = skitai.joinpath ("dep/.protected/.google-application-credential.json")
    if os.path.isfile (cred):
        pref.config.GOOGLE_APPLICATION_CREDENTIALS = cred
    pref.config.AUTH_HANDLER = MyAuthHandler (3600 * 6, 3600 * 24 * 21)


def __app__ ():
    import atila
    return atila.Atila (__name__)

def __setup__ (context, app):
    app.mount ("/api-auth", auth)

def __mount__ (context, app):
    import atila_vue
    @app.route ("/api")
    def api (context):
        return {'version': atila_vue.__version__}

    @app.route ('/')
    @app.route ('/mpa')
    def mpa (context):
        return context.render (
            'mpa.j2',
            vue_config = dict (
                use_loader = True
            ),
            version = atila_vue.__version__
        )

    @app.route ('/spa/<path:path>')
    def spa (context, path = None):
        return context.render (
            'spa.j2',
            vue_config = dict (
                use_router = context.baseurl (spa),
                use_loader = True
            ),
            version = atila_vue.__version__
        )

    @app.route ('/owner/<int:uid>')
    @app.permission_required (["owner"])
    def owner (context, uid):
        return context.API ()

    @app.route ('/owner_staff/<int:uid>')
    @app.permission_required (["owner", "staff"])
    def owner_staff (context, uid):
        return context.API ()

    @app.route ('/anyuser/<int:uid>')
    @app.permission_required
    def anyuser (context, uid):
        return context.API ()

    @app.route ('/staff/<int:uid>')
    @app.permission_required (['staff'])
    def staff (context, uid):
        return context.API ()

    @app.route ('/admin/<int:uid>')
    @app.permission_required (["admin"])
    def admin (context, uid):
        return context.API ()
