#! /usr/bin/env python3

import skitai
import backend
import atila_vue
from atila import Allied
from dotenv import load_dotenv; load_dotenv ()

with skitai.preference () as pref:
    skitai.mount ('/', Allied (atila_vue, backend), pref)

skitai.run (ip = '0.0.0.0', port = 5000, name = 'atila-vue-example', workers = 1)
