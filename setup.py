"""
Hans Roh 2021 -- http://osp.skitai.com
License: MIT
"""

import re
import sys
import os
import shutil, glob
from warnings import warn
try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

with open('atila_vue/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'publish' in sys.argv:
	os.system ('{} setup.py bdist_wheel'.format (sys.executable))
	whl = glob.glob ('dist/atila_vue-{}-*.whl'.format (version))[0]
	os.system ('twine upload {}'.format (whl))
	os.system ('pip3 install -U --pre atila==1000 > /dev/null 2>&1')
	sys.exit ()

classifiers = [
  	'License :: OSI Approved :: MIT License',
  	'Development Status :: 2 - Pre-Alpha',
	'Environment :: Web Environment',
	'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
	'Intended Audience :: Developers',
	'Intended Audience :: Science/Research',
	'Programming Language :: JavaScript',
]

packages = [
	'atila_vue',
	'atila_vue.auth',
]

package_dir = {
	'atila_vue': 'atila_vue'
}

install_requires = [
	'atila',
	'jinja2'
]

def collect (base):
	t = []
	t.append ('{}/*'.format (base [len ('atila_vue/'):]))
	for (path, dirs, files) in os.walk (base):
		if isinstance (dirs, list):
			for d in dirs:
				t.append ('{}/{}/*'.format (path [len ('atila_vue/'):], d))
	return (t)

collected = []
collected.extend (collect ('atila_vue/static'))
collected.extend (collect ('atila_vue/templates'))

package_data = {
	"atila_vue": collected
}


if __name__ == "__main__":
	with open ('README.md', encoding='utf-8') as f:
		long_description = f.read ()

	setup(
		name='atila-vue',
		version=version,
		description='Atila Extension For VueJS 2 SFC and SSR',
		long_description = long_description,
		long_description_content_type = 'text/markdown',
		url = 'https://gitlab.com/atila-ext/atila-vue',
		author='Hans Roh',
		author_email='hansroh@gmail.com',
		packages=packages,
		package_dir=package_dir,
		license='MIT',
		platforms = ["posix",],
		download_url = "https://pypi.python.org/pypi/atila-vue",
		install_requires = install_requires,
		classifiers=classifiers,
		package_data = package_data
	)
