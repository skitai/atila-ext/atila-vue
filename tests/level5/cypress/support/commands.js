Cypress.Commands.add ('signIn', (config) => {
    cy.visit ("/members/signin")
    cy.readFile ('cypress/fixtures/.tokens.json').then ((tokens) => {
        if (tokens.access_token != undefined) {
            cy.window ().then ((win) => {
                win.store.state.$ls.set ('access_token', tokens.access_token)
                win.store.state.$ls.set ('access_token', tokens.refresh_token)
            })
            cy.log ('have got tokens')
        } else {
            cy.log ('getting new tokens...')
            cy.get ('#input-email').eq (0).clear ().type (config.email)
            cy.get ('#input-password').eq (0).type (config.password)
            cy.get ('.actions .btn-primary').eq (0).click ()
            cy.get ('.toast-body').eq (0).contains ('signed in').should ('be.visible')

            cy.window ().then ((win) => {
                const tokens = {
                    access_token: win.store.state.$ls.get ('access_token'),
                    refresh_token: win.store.state.$ls.get ('access_token')
                }
                cy.writeFile ('cypress/fixtures/.tokens.json', tokens)
            })
        }
    })
})

Cypress.Commands.add ('signOut', (config) => {
    cy.visit ("/members/signout")
    cy.get ('.actions .btn-danger').eq (0).click ()
    cy.get ('.toast-body').eq (0).contains ('signed out').should ('be.visible')
    cy.writeFile ('cypress/fixtures/.tokens.json', {})
})

Cypress.Commands.add ('deleteAccount', (config) => {
    cy.visit ("/members/delete")
    cy.get ('#input-password').eq (0).type (config.password)
    cy.get ('.actions .btn-danger').eq (0).click ()
    cy.get ('.toast-body').eq (0).contains ('deleted').should ('be.visible')
    cy.writeFile ('cypress/fixtures/.tokens.json', {})
})
